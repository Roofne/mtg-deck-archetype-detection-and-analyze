##############
# LIBRAIRIES #
##############

import argparse
import glob
from datetime import date,datetime
import os
from src.Model import Model
from src.Analyze import Analyze

##############
# FONCTIONS  #
##############

def getDataFilepath(folder,filter):
    filepath = []
    for filename in glob.glob(folder, recursive=True):
        file_date = filename.split("\\")[4:7]
        file_date = date(int(file_date[0]),int(file_date[1]),int(file_date[2]))
        if (
                (filter["format"] in filename) and
                (filter["start_date"] <= file_date <= filter["end_date"])
            ):
            filepath.append(filename)
    return filepath

##############
# MAIN       #
##############

# Creation du gestionaire d'argument
parser = argparse.ArgumentParser(description="Archetype Analyzer", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("output"  , choices=["console","csv","json"], help="result output format")
parser.add_argument("method"  , choices=['generate','detect','compare'], action="store", help="analyze you want to run")
parser.add_argument("--format", action="store", help="mtg format you want to analyze")
parser.add_argument("--start", action="store", help="Start date like 1/01/90")
parser.add_argument("--end", action="store", help="End date like 1/01/90")

try:
    args = parser.parse_args()

    # handling of the tournament data range
    filter = {
        "format"     : args.format if args.format else "",
        'start_date' : datetime.strptime(args.start, "%d/%m/%y").date() if args.start else date(2022,12,1),
        'end_date'   : datetime.strptime(args.end, "%d/%m/%y").date()   if args.end else date(2022,12,31),
    }
    filepath = getDataFilepath(".\data\Tournaments\mtgo.com\**\*.*",filter)

    # handling of the archetype 
    #archetype    = args.filter if args.filter == '1' else 'Grixis Affinity'
    #include_card = args.filter if args.filter == '1' else 'Grixis Affinity'
    #exclude_card = args.filter if args.filter == '1' else 'Atog'

    # handling of the analyze
    method = args.method if args.method else 'detect'
    a1 = Analyze()
    match method:
        case 'generate':
            a1.loadData(filepath)
            a1.generate()
        case 'detect':
            models = []
            for path in os.listdir("./model/"):
                temp = Model(path.split(".")[0]).load("./model/"+path)
                models.append(temp)
            a1.loadData(filepath)
            a1.detect(models) 
        case 'compare':
            a2 = Analyze()
            a1.compare(a2)

    # handling of the output format
    output = args.output if args.output else 'console'
    match output:
        case 'console':
            a1.toConsole()
        case 'csv':
            a1.toCSV()
        case 'json':
            a1.toJSON()
        case _:
            print("Invalid Output Format")
except argparse.ArgumentError:
    print('Bad Argument')

##############
# HELP       #
##############