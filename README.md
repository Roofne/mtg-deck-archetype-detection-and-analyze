# MTG Deck Archetype Detection and Analyze

Clustering program to detect archetype from MTGO Decklist

## Getting started

- Install puthon 3.10
- Clone the repository
- Once finished, clone the deck repository and rename it to data (https://github.com/Badaro/MTGODecklistCache)

U should have this folder structure
```
project/
    data/
    src/
    model/
    out/
    main.py
```

For using this program, open a terminal and type
```
py main.py console generate --format pauper
py main.py console detect --format pauper
```
The first command will create a folder modern_1 in model that will contain the detected model.
The second command will generate a list containing the deck metadata and a deck archetype from the generated model.

## Output options

You need to choose an output for displaying the results
- console
- csv
- json

If you choose to output to a file it will have the name of the analysis

## Filtering Options

## References

Toine VINTEL [@Roofne]

Inspired by the rule-based system of Badaro (https://github.com/Badaro/MTGOArchetypeParser)