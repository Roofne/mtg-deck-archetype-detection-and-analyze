import json

from src.Deck import Deck

class Tournament:
    # Constructeur 
    def __init__(self, filepath):
        data = self.load(filepath)
        if(data != None):
            self.name   = data["Tournament"]["Name"]
            self.date   = data["Tournament"]["Date"]
            self.url    = data["Tournament"]["Uri"]
            self.format = "pauper"
            self.decks  = []
            for deck in data["Decks"]:
                temp = Deck().parseJSON(deck)
                self.decks.append(temp)

    # Methodes
    def load(self,filepath):
        with open(filepath) as f:
            data = json.load(f)
            f.close()
        return data

    def __str__(self):
        return str(self.name)+" | "+str(self.date)+" | "+str(self.format)+" | "+str(self.url)