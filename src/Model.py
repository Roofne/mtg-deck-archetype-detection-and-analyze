from src.Card import Card
from src.Deck import Deck
import json

class Model(Deck):
    # Attributs

    # Constructeur 
    def __init__(self, name):
        self.name      = name
        self.mainboard = []
        self.sideboard = []

    # Methodes
    def __str__(self):
        temp = "NAME : "+str(self.name)+"\n"
        temp += "\nMAINBOARD\n"
        for c in self.mainboard:
            temp += c.toString()+"\n"
        temp += "\nSIDEBOARD\n"
        for c in self.sideboard:
            temp += c.toString()+"\n"
        return temp

    # on genere un model a partir d'un ensemble de données
    def generate(self, decks):
        names = set()
        for d in decks:
            for c in d.mainboard:
                names.add(c.name)

        # pour chaque carte identique faire la moyenne
        for name in names:
            stat = list()
            for d in decks:
                c = d.getCard(name)
                if c != None:
                    stat.append(c.qty)
            qty = round(sum(stat)/len(stat),2)
            self.setCard(name,qty)
        
        return self

    # on met a jour le model a partir d'une nouvelle donnée
    def update(self, deck):
        for x2 in deck.mainboard:
            name = x2.name
            x1 = self.getCard(name)
            if x1 != None:
                x1.qty = (x1.qty+x2.qty)/2
            else:
                new_x = Card(x2.name,x2.qty)
                self.mainboard.append(new_x)
        return self
            
    # on calcul l'ecart entre la decklist et le model
    def compare(self, deck):
        n1 = len(self.mainboard)
        n2 = len(deck.mainboard)

        c1 = {c.name for c in self.mainboard}
        c2 = {c.name for c in deck.mainboard}

        common_card = c1.intersection(c2)
        other_card = c1.union(c2).difference(common_card)
        
        temp = len(common_card)/n1
        return round(temp,2)

    # methode d'import
    def load(self, filepath):
        file = open(filepath)
        data = json.load(file)
        self.parseJSON(data)
        return self

    # methode d'export
    def save(self, filepath):
        with open(filepath, 'w') as outfile:
            mb=[]; sb=[]
            for c in self.mainboard:
                temp = {"CardName":c.name,"Count":c.qty}
                mb.append(temp)
            for c in self.sideboard:
                temp = {"CardName":c.name,"Count":c.qty}
                sb.append(temp)
            json_data = {
                "name" :self.name,
                "Mainboard":mb,
                "Sideboard":sb
            }
            json.dump(json_data, outfile, indent=1)

