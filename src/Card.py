class Card:
    # Constructeur 
    def __init__(self):
        self.name = ""
        self.qty  = 0

    # Getter et setters
    def getName(self):
        return self.name

    def setQty(self, _qty):
        self.qty = _qty

    # Methode
    def toString(self):
        return str(self.name)+":"+str(self.qty)

    # methode d'import
    def parse(self,_name,_qty):
        self.name = _name
        self.qty  = _qty
        return self

    def parseJSON(self,data):
        self.name = data["CardName"]
        self.qty  = data["Count"]
        return self

    # Methode d'export
    def export():
        return {"CardName":"","Count":""}
