from src.Card import Card

class Deck:
    # Constructeur 
    def __init__(self):
        self.result    = ""
        self.url       = ""
        self.player    = ""
        self.mainboard = []
        self.sideboard = []

    # Getter et setters
    def getCard(self, name):
        for c in self.mainboard:
            if c.name == name: 
                return c
        return None 

    def setCard(self, name, qty):
        for c in self.mainboard:
            if c.name == name: 
                c.qty = qty
                exit()
        self.mainboard.append(Card().parse(name,qty))

    # Methode
    def __str__(self):
        temp = "NAME : "+str(self.player)+"\n"
        temp += "\nMAINBOARD\n"
        for c in self.mainboard:
            temp += c.toString()+"\n"
        temp += "\nSIDEBOARD\n"
        for c in self.sideboard:
            temp += c.toString()+"\n"
        return temp
    
    def compare(self, other):
        n1 = len(self.mainboard)
        n2 = len(other.mainboard)

        c1 = {c.name for c in self.mainboard}
        c2 = {c.name for c in other.mainboard}

        common_card = c1.intersection(c2)
        other_card = c1.union(c2).difference(common_card)
        
        temp = len(common_card)/n1
        return round(temp,2)

    # Fonction pour adapter la classe au nouvelel données 
    def parseData(self,_p,_mb,_sb):
        self.player    = _p
        self.mainboard = _mb
        self.sideboard = _sb
        return self

    def parseJSON(self, data):
        if(data != None):
            self.date   = data["Date"] if "Date" in data.keys() else ""
            self.result = data["Result"] if "Result" in data.keys() else ""
            self.url    = data["AnchorUri"] if "AnchorUri" in data.keys() else ""
            self.player = data["Player"] if "Player" in data.keys() else ""
            for card in data["Mainboard"]:
                temp = Card().parseJSON(card)
                self.mainboard.append(temp)
            for card in data["Sideboard"]:
                temp = Card().parseJSON(card)
                self.sideboard.append(temp)
        return self


    # Trie le deck selon le nombre d'exemplaire des cartes, leur nom ou leur type
    def sort(self):
        cards = self.mainboard; n=len(cards); d={}; temp=[]
        # on trie avec l'aide dun dictionnaire index:poids
        for i in range(0,n):
            d[i] = cards[i].qty
        d = dict(sorted(d.items(), key=lambda x: x[1],reverse=True))
        # on trie le mainboard
        for key in d.keys():
            temp.append(cards[key])
        self.mainboard=temp
        return self
