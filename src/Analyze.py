import csv
import numpy as np
from src.Model import Model
from src.Tournament import Tournament

class Analyze:
    # Constructeur
    def __init__(self):
        self.name = "" 
        self.date = "01/01/1990"
        self.data   = []
        self.model  = []
        self.result = []

    # Methode

    # Methode d'import
    def loadData(self, filepath):
        for f in filepath:
            temp = Tournament(f)
            self.data.append(temp)

    def loadModel(self, filepath):
        for f in filepath:
            temp = Model()
            self.model.append(temp)

    # Methode d'export
    def toConsole(self):
        print(self.result)

    def toCSV(self):
        with open("./out/result.csv",'w',newline='') as file:
            writer = csv.writer(file, delimiter=",")
            writer.writerows(self.result)

    def toJSON(self):
        print(self.result)

    # Methode de calcul
    def generate(self):
        # on recupere les decks sous forme de liste
        decks = []
        for tournament in self.data:
            for deck in tournament.decks:
                decks.append(deck)

        #pour chaque deck on cherche un element similaire sinon il fait son groupe
        n = len(decks); keys=[]; groups={}
        for i in range(0,n):
            r_max=-1; r_j=-1
            for j in keys:
                r = decks[i].compare(decks[j])
                if r>r_max and i!=j and r>0.6:
                    r_max = r
                    r_j = j
            if r_j == -1:
                keys.append(i)
                groups[i] = [i]
            else:
                groups[r_j].append(i)

        #on creer les models
        models = []
        for i in groups.keys():
            temp = []
            for j in groups[i]:
                temp.append(decks[i])
            m = Model(str(i)).generate(temp).sort()
            models.append(m)

        # on exporte les models
        for m in models:
            m.save("./model/"+m.name+".json")
            self.result.append([m.name,"new archetype"])

    def detect(self, models):
        # on recupere les decks sous forme de liste
        decks = []
        for tournament in self.data:
            for deck in tournament.decks:
                decks.append(deck)

        # Pour chaque deck, je fait une detection selon chaque model
        self.result = [["DATE","RESULT","PLAYER","URL","ARCHETYPE"]]
        for deck in decks:
            r_max=0; r_m=-1
            for m in models:
                r = m.compare(deck)
                if r > r_max:
                    r_max = r
                    r_m = m 
            if r_max > 0.6:
                temp = [deck.date,deck.result,deck.player,deck.url,r_m.name]
            else:
                temp = [deck.date,deck.result,deck.player,deck.url,"Other"]
            self.result.append(temp)   

    def compare(self, other_analyze):
        print("Je recupere tous les decks des tournois")
        print("Je recupere les models de l'analyse 1")
        print("Je recupere les models de l'analyse 2")
        print("Pour chaque deck, je fait une detection selon les models 1 et 2")
        print("Je marque les resultat de la comparaison (deck,resultat 1,resultat 2, diff)")